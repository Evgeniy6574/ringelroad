﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="LvlSetting",menuName ="Settings/LvlSettings")]
public class LvlSetting : ScriptableObject
{
    public float PlayerSpeed;
    public float ObstacleSpeed;
    public float EnemyesSpeed;
    public int CirclesCount;

   public List<ObstacleElement> ObstacleElements;
}