﻿
using System.Collections;
using UnityEngine;

public enum ObstacleType
{
    Null,
    FanType1,
    FanType2,
    FanType3,
    FanType4,
    Enemy
}
public class Obstacle : MonoBehaviour
{
    public ObstacleType ObstacleType;
    [SerializeField] private MoveController _MoveController;

    public void SetData(bool isRight = true, float speed = 1)
    {
        _MoveController.SetData(isRight, speed);
        _MoveController.Activ();
    }
}