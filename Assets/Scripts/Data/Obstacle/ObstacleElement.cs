﻿[System.Serializable]
public class ObstacleElement
{
    public int Weight;
    public Obstacle Obstacle;
}