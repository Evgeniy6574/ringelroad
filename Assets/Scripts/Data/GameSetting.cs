﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSetting", menuName = "Settings/GameSetting")]
public class GameSetting : ScriptableObject
{
    public Vector2 GameWeight;
    public Vector2 LvlRange;
    public Vector2 UpStep;
    public Vector2 DownSpep;
}