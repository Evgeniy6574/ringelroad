﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ringel : MonoBehaviour
{
    public bool IsRight { get; private set; }
    public int Weigth { get; private set; }
    [SerializeField] private Transform _Container;

    private float _ObstacleSpeed;
    private float _EnemySpeed;

    [SerializeField]private List<Obstacle> _Collections;


    public void SetData(int weight, bool isRight = true, float obstacleSpeed = 1, float enemySpeed = 1)
    {
        IsRight = isRight;
        Weigth = weight;
        _ObstacleSpeed = obstacleSpeed;
        _EnemySpeed = enemySpeed;
    }
    public void Add(ObstacleElement obstacleElement)
    {
        if (_Collections == null)
        {
            _Collections = new List<Obstacle>();
        }
        if (obstacleElement.Obstacle.ObstacleType == ObstacleType.Enemy)
        {
            _Collections.Add(Instantiate(obstacleElement.Obstacle, _Container.position,
                Quaternion.Euler(Vector3.up * Random.Range(0, 180)),_Container));
        }
        else
        {
            _Collections.Add(Instantiate(obstacleElement.Obstacle, _Container));
        }
        Weigth -= obstacleElement.Weight;
    }
    public void Activate()
    {
        if (_Collections.Count > 0)
        {
            for (int i = 0; i < _Collections.Count; i++)
            {
                if (_Collections[i].ObstacleType == ObstacleType.Enemy)
                {
                    _Collections[i].SetData(IsRight, _EnemySpeed);
                }
                else
                {
                    _Collections[i].SetData(IsRight, _ObstacleSpeed);
                }
            }
        }
    }
}
