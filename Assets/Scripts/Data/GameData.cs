﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public int CurrentLvl = 1;
    public GameSetting GameSetting;
    public LvlSetting LvlSetting;
    public ScreenData ScreenData;
}
