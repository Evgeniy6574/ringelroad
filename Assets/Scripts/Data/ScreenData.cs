﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
[System.Serializable]
public class ScreenData
{
    public int CurrentRingel
    {
        get { return _CurrentRingel; }
        set
        {
            _CurrentRingel = value;
            ChangeCurrentRingelAction?.Invoke(_CurrentRingel);
        }
    }
    public UnityAction<int> ChangeCurrentRingelAction;
    private int _CurrentRingel;

    public int MaxRingel
    {
        get { return _MaxRingel; }
        set
        {
            _MaxRingel = value;
            ChangeMaxRingelAction?.Invoke(_MaxRingel);
        }
    }
    public UnityAction<int> ChangeMaxRingelAction;
    private int _MaxRingel;
}
