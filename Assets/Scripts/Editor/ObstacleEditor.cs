﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Obstacle))]
public class ObstacleEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Obstacle obstacle = (Obstacle)target;
        if (GUILayout.Button("Activ"))
        {
            obstacle.SetData();
        }
    }
}
