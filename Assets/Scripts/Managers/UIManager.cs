﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField] private List<UIScreen> _UIScreens;

    public UIScreen OpenScreen(ScreenID screenID)
    {
        UIScreen uIScreenl;
        for (int i = 0; i < _UIScreens.Count; i++)
        {
            _UIScreens[i].Close();
        }
        uIScreenl = _UIScreens.Find(e => e.ScreenID == screenID).Open();
        return uIScreenl;
    }
}
