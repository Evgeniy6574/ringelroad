﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSystem : MonoBehaviour
{
    public static GameSystem instance;

    [SerializeField] private GameManager _GameManager;
    private void Awake()
    {
        instance = this;
    }

    public void StartGame()
    {
        _GameManager.StartGame();
    }
    public void RestartGame()
    {
        _GameManager.RestartGame();
    }
    public void NextLvl()
    {
        _GameManager.NextLvl();
    }
}
