﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    Pause,
    Game,
    GameOver
}
public class GameManager : MonoBehaviour
{
    public GameState GameState { get; private set; } = GameState.Pause;

    [SerializeField] private GameData _GameData;
    [SerializeField] private UIManager _UIManager;
    [SerializeField] private InputController _InputController;
    [SerializeField] private LvlGenerator _LvlGenerator;
    [SerializeField] private CameraControl _CameraControl;
    [SerializeField] private Player _Player;

    private bool _IsActivPortal;
    private int _CurrentRingel;
    private int _CountLvlToUp;
    private int _CountLvlToDown;
    private List<Ringel> _Ringels;

    #region Unity
    void Start()
    {
        _InputController.StartPresedAcrion += StartPresed;
        _InputController.EndPresedAcrion += EndPresed;

        _Player.ActivFinishAction += Finish;
        _Player.ActivPotalAction += Portal;
        _Player.DethAction += GameOver;
        _CountLvlToUp = (int)_GameData.GameSetting.UpStep.x;
        _CountLvlToDown = (int)_GameData.GameSetting.DownSpep.x;

        _UIManager.OpenScreen(ScreenID.Pause);
    }

    #endregion

    public void StartGame()
    {
        if (GameState == GameState.Pause)
        {
            GenereateLvl();
            _GameData.ScreenData.MaxRingel = _GameData.LvlSetting.CirclesCount;
            SetRingel();

            _UIManager.OpenScreen(ScreenID.Game).SetData(_GameData.ScreenData);

            GameState = GameState.Game;
        }
    }
    public void NextLvl()
    {
        _GameData.CurrentLvl++;
        CheckSettings();
        StartGame();
    }
    public void RestartGame()
    {
        if (GameState == GameState.GameOver)
        {
            GameState = GameState.Pause;
            StartGame();
        }
    }

    private void GameOver()
    {
        if (GameState == GameState.Game)
        {
            GameState = GameState.GameOver;
            EndPresed();
            _UIManager.OpenScreen(ScreenID.GameOver);
        }
    }

    private void GenereateLvl()
    {
        _CurrentRingel = 0;
        _Ringels = _LvlGenerator.GenerateLvl(_GameData.CurrentLvl, _GameData.LvlSetting, _GameData.GameSetting.LvlRange);
    }
    private void SetRingel()
    {
        _Player.SetPoint(_Ringels[_CurrentRingel].transform.position);
        _CameraControl.SetTarget(_Ringels[_CurrentRingel].transform.position);
        _Player.SetData(_Ringels[_CurrentRingel].IsRight, _GameData.LvlSetting.PlayerSpeed);
        _GameData.ScreenData.CurrentRingel = _CurrentRingel;
    }

    private void EndPresed()
    {
        if (GameState == GameState.Game)
        {
            _Player.StopMove();
        }
        if (_IsActivPortal)
        {
            _CurrentRingel++;
            SetRingel();
        }
    }
    private void StartPresed()
    {
        if (GameState == GameState.Game)
        {
            _Player.StartMove();
        }
    }

    private void Portal(bool value)
    {
        _IsActivPortal = value;
    }
    private void Finish()
    {
        if (GameState == GameState.Game)
        {
            _UIManager.OpenScreen(ScreenID.CompliteLvl);
            GameState = GameState.Pause;
        }
    }

    private void CheckSettings()
    {
        _CountLvlToDown--;
        _CountLvlToUp--;
        if (_CountLvlToUp <= 0)
        {
            _GameData.GameSetting.LvlRange.x += _GameData.GameSetting.UpStep.y;
            if (_GameData.GameSetting.LvlRange.x > _GameData.GameSetting.LvlRange.y)
            {
                _GameData.GameSetting.LvlRange.x = _GameData.GameSetting.LvlRange.y;
            }
            _CountLvlToUp = (int)_GameData.GameSetting.UpStep.x;
        }
        if (_CountLvlToDown <= 0)
        {
            _GameData.GameSetting.LvlRange.x -= _GameData.GameSetting.DownSpep.y;
            if (_GameData.GameSetting.LvlRange.x < _GameData.GameSetting.GameWeight.x)
            {
                _GameData.GameSetting.LvlRange.x = _GameData.GameSetting.GameWeight.x;
            }
            _CountLvlToDown = (int)_GameData.GameSetting.DownSpep.x;
        }
    }
}