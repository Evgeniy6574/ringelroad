﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LvlGenerator : MonoBehaviour
{
    [SerializeField] private float _Step;
    [SerializeField] private Transform _Container;
    [SerializeField] private Ringel _RingelPrefab;
    [SerializeField] private GameObject _PortalPrefab;
    [SerializeField] private GameObject _FinishPrefab;

    private System.Random _Random;
    private ObstacleType _LastObstacle;

    public List<Ringel> GenerateLvl(int Lvl, LvlSetting lvlSetting, Vector2 weigth)
    {
        bool isright = true;
        List<Ringel> collection = new List<Ringel>();
        Ringel ringel = new Ringel();
        ObstacleElement obstacleElement;
        _Random = new System.Random(Lvl);
     
        ClearLvl();

        for (int i = 0; i < lvlSetting.CirclesCount; i++)
        {
            _LastObstacle = ObstacleType.Null;
            ringel = Instantiate(_RingelPrefab, (Vector3.forward * (_Step * i)), Quaternion.identity, _Container);
            ringel.SetData(_Random.Next((int)weigth.x, (int)weigth.y), isright, lvlSetting.ObstacleSpeed, lvlSetting.EnemyesSpeed);
            isright = !isright;
            if (i < lvlSetting.CirclesCount - 1)
            {
                Instantiate(_PortalPrefab, ringel.transform);
            }
            else
            {
                Instantiate(_FinishPrefab, ringel.transform);
            }
            if (i > 0)
            {
                do
                {
                    obstacleElement = GenerateObstacle(ringel.Weigth, lvlSetting.ObstacleElements);
                    if (obstacleElement != null)
                    {
                        ringel.Add(obstacleElement);
                    }
                } while (obstacleElement != null);
            }
            collection.Add(ringel);
            ringel.Activate();
        }
        return collection;
    }

    public void ClearLvl()
    {
        int size = _Container.childCount;
        Transform temp;
        for (int i = 0; i < size; i++)
        {
            temp = _Container.GetChild(0);
            temp.parent = null;
            Destroy(temp.gameObject);
        }
    }

    private ObstacleElement GenerateObstacle(int weigthLimit, List<ObstacleElement> obstacleElements)
    {
        ObstacleElement obstacleElement = new ObstacleElement();
        List<ObstacleElement> temp = obstacleElements.FindAll(e => e.Weight <= weigthLimit);
        if (temp.Count > 0)
        {
            if (_LastObstacle != ObstacleType.Null)
            {
                if (_LastObstacle == ObstacleType.Enemy)
                {
                    temp.RemoveAll(e => e.Obstacle.ObstacleType != ObstacleType.Enemy);
                }
                else
                {
                    temp.RemoveAll(e => e.Obstacle.ObstacleType == ObstacleType.Enemy);
                }
            }
            if (temp.Count > 0)
            {
                obstacleElement = temp[_Random.Next(0, temp.Count)];
            }
            if (obstacleElement.Obstacle != null)
            {
                _LastObstacle = obstacleElement.Obstacle.ObstacleType;
                return obstacleElement;

            }
        }
        return null;
    }
}

