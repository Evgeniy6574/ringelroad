﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PauseScreen : UIScreen
{
    [SerializeField] private Button _StartButton;

    private bool _IsSetData;
    public override UIScreen Open()
    {
        if (!_IsSetData)
        {
            _StartButton.onClick.AddListener(() =>
            {
                GameSystem.instance.StartGame();
            });
            _IsSetData = true;
        }
        return base.Open();
    }
}
