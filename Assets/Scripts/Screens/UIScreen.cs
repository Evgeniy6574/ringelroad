﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ScreenID
{
    Pause,
    Game,
    CompliteLvl,
    GameOver
}
public class UIScreen : MonoBehaviour
{
    public ScreenID ScreenID;

    public virtual UIScreen Open()
    {
        this.gameObject.SetActive(true);
        return this;
    }
    public virtual void Close()
    {
        this.gameObject.SetActive(false);
    }
    public virtual void SetData(ScreenData screenData)
    {

    }
}
