﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScreen : UIScreen
{
    [SerializeField] private Button _RestartButton;

    private bool _IsSetData;
    public override UIScreen Open()
    {
        if (!_IsSetData)
        {
            _RestartButton.onClick.AddListener(() =>
            {
                GameSystem.instance.RestartGame();
            });
            _IsSetData = true;
        }
        return base.Open();
    }
}
