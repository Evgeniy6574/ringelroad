﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameScreen : UIScreen
{
    [SerializeField] private Text _CurrenProgressText;
    [SerializeField] private Text _MaxProgressText;


    private bool _IsSetData;
    public override UIScreen Open()
    {

        return base.Open();
    }
    private void UpdateCurrentProgres(int currentValue)
    {
        _CurrenProgressText.text = "" + currentValue;
    }
    private void UpdateMaxProgres(int maxValue)
    {
        _MaxProgressText.text = "" + maxValue;
    }
    public override void SetData(ScreenData screenData)
    {
        UpdateCurrentProgres(screenData.CurrentRingel);
        UpdateMaxProgres(screenData.MaxRingel);
        if (!_IsSetData)
        { 
            screenData.ChangeCurrentRingelAction += UpdateCurrentProgres;
            screenData.ChangeMaxRingelAction += UpdateMaxProgres;
        }
    }
}
