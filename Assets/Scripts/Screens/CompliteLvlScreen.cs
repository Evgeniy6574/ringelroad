﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CompliteLvlScreen : UIScreen
{
    [SerializeField] private Button _NextLvlButton;

    private bool _IsSetData;
    public override UIScreen Open()
    {
        if (!_IsSetData)
        {
            _NextLvlButton.onClick.AddListener(() =>
            {
                GameSystem.instance.NextLvl();
            });
            _IsSetData = true;
        }
        return base.Open();
    }
}
