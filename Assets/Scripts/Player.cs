﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class Player : MonoBehaviour
{
    public UnityAction<bool> ActivPotalAction;
    public UnityAction ActivFinishAction;
    public UnityAction DethAction;


    [SerializeField] private MoveController _MoveController;
    [SerializeField] private CollisionAction _CollisionAction;

    private bool _IsActiv;
    private int _Direction = 1;
    private float _Speed = 1;

    private Coroutine _CoroutineMovement;

    #region Unity
    private void Awake()
    {
        _CollisionAction.TriggerEnter += TriggerEnter;
        _CollisionAction.TriggerExit += TriggerExit;

    }

    #endregion
    public void StartMove()
    {
        _MoveController.Activ();
    }
    public void StopMove()
    {
        _MoveController.Deactiv();
    }
    public void SetData(bool isRight, float speed = 1)
    {
        _MoveController.SetData(isRight, speed);
    }
    public void SetPoint(Vector3 point)
    {
        transform.rotation = Quaternion.identity;
        transform.position = point;
    }

    private void TriggerEnter(Collider collider)
    {
        Debug.Log("Enter: " + collider.tag);
        if (collider.tag == "Portal")
        {
            ActivPotalAction?.Invoke(true);
        }
        if (collider.tag == "Finish")
        {
            ActivFinishAction?.Invoke();
        }
        if (collider.tag == "Enemy")
        {
            DethAction?.Invoke();
        }
    }
    private void TriggerExit(Collider collider)
    {
        if (collider.tag == "Portal")
        {
            ActivPotalAction?.Invoke(false);
        }
    }
}
