﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class InputController : MonoBehaviour
{
   public UnityAction StartPresedAcrion;
   public UnityAction EndPresedAcrion;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StartPresedAcrion?.Invoke();
        }
        if (Input.GetMouseButtonUp(0))
        {
            EndPresedAcrion?.Invoke();
        }
    }
}
