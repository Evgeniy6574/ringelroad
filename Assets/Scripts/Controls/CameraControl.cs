﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{

    [SerializeField] private Vector3 _Distance;
    [SerializeField] private float _Speed;
    private Vector3 target;

    private Coroutine _CoroutineMoved;
    public void SetTarget(Vector3 point)
    {
        target = point + _Distance;
        if(_CoroutineMoved!=null)
        {
            StopCoroutine(_CoroutineMoved);
        }
        _CoroutineMoved = StartCoroutine(Moved());
    }

    private IEnumerator Moved()
    {
        while(Vector3.Distance(transform.position,target)>0.05f)
        {
            yield return new WaitForFixedUpdate();
            transform.position = Vector3.Lerp(transform.position, target, Time.deltaTime * _Speed);
        }
    }
}
