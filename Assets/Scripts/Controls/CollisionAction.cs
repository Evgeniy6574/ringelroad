﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class CollisionAction : MonoBehaviour
{
    public UnityAction<Collider> TriggerEnter;
    public UnityAction<Collider> TriggerExit;

    private void OnTriggerEnter(Collider other)
    {
        TriggerEnter?.Invoke(other);
    }
    private void OnTriggerExit(Collider other)
    {
        TriggerExit?.Invoke(other);
    }
}
