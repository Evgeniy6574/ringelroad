﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour
{
    [SerializeField] private Transform _Pivot;

    private bool _IsActiv;
    private int _Direction = 1;
    private float _Speed = 1;

    private Coroutine _CoroutineMovement;

    public void SetData(bool isRight, float speed = 1)
    {
        _Direction = isRight ? 1 : -1;
        _Speed = speed;
    }
    public void Activ()
    {
        Deactiv();
        _IsActiv = true;
        _CoroutineMovement = StartCoroutine(Movement());
    }
    public void Deactiv()
    {
        if (_CoroutineMovement != null)
        {
            StopCoroutine(_CoroutineMovement);
            _IsActiv = false;
        }
    }

    private IEnumerator Movement()
    {
        while (_IsActiv)
        {
            yield return new WaitForFixedUpdate();
            _Pivot.rotation *= Quaternion.Euler(Vector3.up * (_Speed * _Direction));
        }
    }
}
